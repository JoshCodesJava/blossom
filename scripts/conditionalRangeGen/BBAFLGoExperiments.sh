#!/bin/bash --posix

# usage: ./BBAFLGoExperiments.sh 25 25 5 
# params: blackbox timeout (in minutes), AFLGo timeout (in minutes), iterations

# generate result directory
if [ ! -d ../../results ]; then
  mkdir ../../results
  mkdir ../../results/blackbox+aflgo
elif [ ! -d ../../results/blackbox+aflgo ]; then
  mkdir ../../results/blackbox+aflgo
fi

timeout=$1m+$2m

declare -a benchmarks=(
  "arclength" \
	"fbenchV1" \
	"fbenchV2" \
	"invertedPendulum"\
	"linearSVC" \
	"linpack" \
	"nbody" \
	"rayCasting" \
	"lulesh"
)

GR_DIR="../../../blackbox"
BR_DIR="../aflgo/broadenRange"

BB_RESULTS_DIRECTORY="../../../results/blackbox+aflgo"
AFLGO_RESULTS_DIRECTORY="../../../../../results/blackbox+aflgo"		
cd ../../src/generateRange/blackbox
for ((i=1;i<=$3;i++)) # $3: iterations 
do
	for file in "${benchmarks[@]}"
  	do
		echo "****************************** Benchmark:" ${file} "******************************"
		echo "****************************** Generating initial ranges with blackbox ******************************"
		if [ ${file} == "lulesh" ]; then
	    g++ lulesh.cc -O3 -lm
		elif [ ${file} == "nbody" ]; then
			g++ nbody.c -O3 -lm
		else
	    gcc ${file}.c -O3 -lm
		fi

		# generate result directory
		if [ ! -d $BB_RESULTS_DIRECTORY/${file}${timeout} ]
		then
	    mkdir $BB_RESULTS_DIRECTORY/${file}${timeout}
		fi
		if [ ! -d $BB_RESULTS_DIRECTORY/${file}${timeout}/ex$i ]
		then
	    mkdir $BB_RESULTS_DIRECTORY/${file}${timeout}/ex$i
		fi

		seed=$(($RANDOM%1000))
		echo ${seed} >> $BB_RESULTS_DIRECTORY/${file}${timeout}/ex$i/seed.txt
    	./a.out ${seed} $1 # $1: blackbox timeout

		# save the results
    	cp kernel* $BB_RESULTS_DIRECTORY/${file}${timeout}/ex$i/
		# Copy results for broaden_ranges
		mv kernel* $BR_DIR/${file}

    	# runs aflgo to broaden the initial ranges
    	echo "****************************** Broadening the range with AFLGo ******************************"
		cd $BR_DIR/${file}
		./aflgo.sh $2 # $2: AFLGo timeout
		cp build/broad_kernel* $AFLGO_RESULTS_DIRECTORY/${file}${timeout}/ex$i/
		cp build/in/in $AFLGO_RESULTS_DIRECTORY/${file}${timeout}/ex$i/in_GR.dat
	
		cd $GR_DIR
  	done
done
