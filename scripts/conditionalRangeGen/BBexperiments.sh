#!/bin/bash --posix

# usage: ./BBexperiments.sh 50 5 
# params: blackbox timeout (in minutes), iterations

# generate result directory
if [ ! -d ../../results ]; then
  mkdir ../../results
  mkdir ../../results/blackbox
elif [ ! -d ../../results/blackbox ]; then
  mkdir ../../results/blackbox
fi

RANGE_DIRECTORY="../../src/generateRange/blackbox"
RESULTS_DIRECTORY="../../../results/blackbox"

declare -a benchmarks=("arclength" \
  "fbenchV1" \
  "fbenchV2" \
  "rayCasting" \
  "linearSVC" \
  "invertedPendulum"\
  "linpack"\
  "nbody" \
  "lulesh")

echo "******************** Range generation with Black-box testing ********************"
# runs blackbox test to generate the ranges

cd $RANGE_DIRECTORY
for file in "${benchmarks[@]}"
do

  if [ ${file} == "nbody" ]; then
    g++ ${file}.c -O3 -lm
  elif [ ${file} == "lulesh" ]; then
    g++ ${file}.cc -O3 -lm
  else
    gcc ${file}.c -O3 -lm  
  fi

  # run the blackbox fuzzing
  echo ${file}
  for ((i=1;i<=$2;i++))
  do
    # generate result directory
    if [ ! -d $RESULTS_DIRECTORY/${file}$1m ]; then
      mkdir $RESULTS_DIRECTORY/${file}$1m
    fi
    if [ ! -d $RESULTS_DIRECTORY/${file}$1m/ex$i ]; then
      mkdir $RESULTS_DIRECTORY/${file}$1m/ex$i
    fi
    seed=$(($RANDOM%1000))
    echo ${seed} >> $RESULTS_DIRECTORY/${file}$1m/ex$i/seed.txt
    ./a.out ${seed} $1 # $1 : timeout

    # save the results
    mv kernel* $RESULTS_DIRECTORY/${file}$1m/ex$i/
  done
done
