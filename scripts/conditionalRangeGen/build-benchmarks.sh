#!/bin/bash
#usage is to run the script with 1 parameter, which is the time in minutes that the scipt should run, in the script directory
rust_benchmarks=('arclength_rust' 'linearSVC_rust' 'lulesh_rust' 'invertedPendulum_rust' 'rayCasting_rust' 'nbody_rust')
cpp_benchmarks=('lulesh')
c_benchmarks=('arclength' 'fbenchV1' 'fbenchV2' 'invertedPendulum'
'linearSVC' 'linpack' 'nbody' 'rayCasting')

mkdir -p busy-files
mkdir -p build

runtime=$1
seed=$RANDOM
RUST_DIR=$(find ~/.rustup -wholename "*/lib/libstd*.so" -and -not -wholename "*rustlib*")
echo "Seed is $seed"

for i in "${cpp_benchmarks[@]}"
do
	clang++ -emit-llvm -c ../../src/generateRange/black-box-pass/Benchmarks/c++/$i.cc -o busy-files/$i.bc
	opt -load ../../src/generateRange/black-box-pass/built/black_box_pass.so -blackbox  -time ${runtime} -seed ${seed} -lang c++ -config ../../src/generateRange/black-box-pass/Benchmarks/c++/$i.config < busy-files/${i}.bc > busy-files/${i}_transform.bc 2>/dev/null
	llc -filetype=obj busy-files/${i}_transform.bc
	clang++ busy-files/${i}_transform.o -o build/${i}.out
done

for i in "${c_benchmarks[@]}"
do
        clang -emit-llvm -c ../../src/generateRange/black-box-pass/Benchmarks/c/$i.c -o busy-files/$i.bc
        opt -load ../../src/generateRange/black-box-pass/built/black_box_pass.so -blackbox -time ${runtime} -seed ${seed} -lang c -config ../../src/generateRange/black-box-pass/Benchmarks/c/$i.config < busy-files/${i}.bc > busy-files/${i}_transform.bc 2>/dev/null
	llc -filetype=obj busy-files/${i}_transform.bc
        clang busy-files/${i}_transform.o -o build/${i}.out -lm
done

for i in "${rust_benchmarks[@]}"
do
        rustc --crate-type lib -g --emit=llvm-bc ../../src/generateRange/black-box-pass/Benchmarks/rust/$i.rs -o busy-files/$i.bc
        opt -load ../../src/generateRange/black-box-pass/built/black_box_pass.so -blackbox -time ${runtime} -seed ${seed} -lang rust -config ../../src/generateRange/black-box-pass/Benchmarks/rust/$i.config < busy-files/${i}.bc > busy-files/${i}_transform.bc 2>/dev/null
	llc -filetype=obj busy-files/${i}_transform.bc
	clang++ -Xlinker $RUST_DIR busy-files/${i}_transform.o -o build/${i}.out
done

rm -r busy-files
