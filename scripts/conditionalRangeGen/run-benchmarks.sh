#usage is to run the script with no parameters in the script directory
#!/bin/bash

benchmarks=('arclength' 'fbenchV1' 'fbenchV2' 'invertedPendulum'
'linearSVC' 'linpack' 'nbody' 'rayCasting' 'lulesh' 'arclength_rust' 'linearSVC_rust' 'lulesh_rust' 'invertedPendulum_rust' 'rayCasting_rust' 'nbody_rust')

mkdir -p results
for i in "${benchmarks[@]}"
do
        build/${i}.out > results/${i}.log
done

