#!/bin/bash --posix
# usage: ./experimentsCBMC.sh 60
# param: timeout (in minutes)

declare -a benchmarks=("../../src/stateOfTheArt/cbmc/arclength.c" \
  "../../src/stateOfTheArt/cbmc/rayCasting.c" \
  "../../src/stateOfTheArt/cbmc/linearSVC.c" \
  "../../src/stateOfTheArt/cbmc/nbody.c" \
  "../../src/stateOfTheArt/cbmc/fbenchV1.c"\
  "../../src/stateOfTheArt/cbmc/fbenchV2.c"\
  "../../src/stateOfTheArt/cbmc/lulesh.cc"\
  "../../src/stateOfTheArt/cbmc/invertedPendulum.c")

TIMELIMIT=$((1 * 60))

#finds the cbmc executable
CBMC=$(find / ! -path '/proc' -type f -executable -name "cbmc")

# Runs CBMC on the whole code
echo "******************** CBMC Results ********************"
for file in "${benchmarks[@]}"
do
  echo ${file}
  timeout --preserve-status ${TIMELIMIT} ${CBMC} --LP64 --drop-unused-functions --unwind 50 $file
done
