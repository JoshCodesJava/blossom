#!/bin/bash --posix
# usage: ./experimentsKLEE.sh 60 
# param: timeout (in minutes)

declare -a benchmarks=("../../src/stateOfTheArt/klee-float/arclength.c" \
  "../../src/stateOfTheArt/klee-float/rayCasting.c" \
  "../../src/stateOfTheArt/klee-float/linearSVC.c" \
  "../../src/stateOfTheArt/klee-float/nbody.c" \
  "../../src/stateOfTheArt/klee-float/linpack.c" \
  "../../src/stateOfTheArt/klee-float/fbenchV1.c" \
  "../../src/stateOfTheArt/klee-float/fbenchV2.c"\
  "../../src/stateOfTheArt/klee-float/invertedPendulum.c")

TIMELIMIT=$((1 * 60))

# Runs Klee-Float on the whole code
echo "******************** Klee-Float Results ********************"
for file in "${benchmarks[@]}"
do
  echo ${file}
  clang -g -emit-llvm -O3 -c $file 
  timeout --preserve-status ${TIMELIMIT} klee --posix-runtime -libc=uclibc -exit-on-error -link-llvm-lib=/home/user/uclibc/lib/libm.a a.bc -sys-arg 2
done
