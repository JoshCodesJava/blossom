/* Code taken from Rosetta Code (https://rosettacode.org/wiki/N-body_problem#C).
  Simulates the interaction of several masses under gravity. 
  Takes as input a configuration file specifying the number of masses and their
  positions, in the following format:
  <Gravitational Constant> <Number of bodies(N)> <Time step>
  <Mass of M1>
  <Position of M1 in x,y,z co-ordinates>
  <Initial velocity of M1 in x,,y,z components>
  ...
  <And so on for N bodies>
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdbool.h> 

typedef struct{
  double x,y,z;
} vector;

#define bodies 3
double GravConstant = 0.01;
int timeSteps = 10;
double masses[3];
vector *positions = (vector*)malloc(bodies*sizeof(vector));
vector *velocities = (vector*)malloc(bodies*sizeof(vector));
vector *accelerations = (vector*)malloc(bodies*sizeof(vector));

/* File to print the kernel inputs */
FILE *fptr_kernel1;
FILE *fptr_kernel2;

/* Generating random doubles */
double get_random(double lowerBound, double upperBound) {
  double f = (double)rand() / RAND_MAX;
  return lowerBound + f * (upperBound - lowerBound);
}

vector addVectors(vector a,vector b) {
  vector c = {a.x+b.x,a.y+b.y,a.z+b.z};

  return c;
}

vector scaleVector(double b,vector a) {
  vector c = {b*a.x,b*a.y,b*a.z};

  return c;
}

vector subtractVectors(vector a,vector b) {
  vector c = {a.x-b.x,a.y-b.y,a.z-b.z};

  return c;
}

double mod(vector a) {
  return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

void numerical_kernel2() {
  /* Keeping track of the input points */
   fprintf(fptr_kernel1,"%.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e\n", 
     velocities[0].x, velocities[0].y, velocities[0].z,
     velocities[1].x, velocities[1].y, velocities[1].z,
     velocities[2].x, velocities[2].y, velocities[2].z,
     accelerations[0].x, accelerations[0].y, accelerations[0].z,
     accelerations[1].x, accelerations[1].y, accelerations[1].z,
     accelerations[2].x, accelerations[2].y, accelerations[2].z);
/* =========================================================== */
  for(int i=0;i<bodies;i++) {
    velocities[i] = addVectors(velocities[i],accelerations[i]); 
  }
}

vector numerical_kernel1(double mass, vector position_i, vector position_j, vector acceleration) {
/* Keeping track of the input points */
  fprintf(fptr_kernel2,"%.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e %.20e\n", 
    position_i.x, position_i.y, position_i.z,
    position_j.x, position_j.y, position_j.z,
    acceleration.x, acceleration.y, acceleration.z);
/* =========================================================== */

  vector acceleration_computed = addVectors(acceleration,scaleVector(GravConstant*mass/
  pow(mod(subtractVectors(position_i,position_j)),3),subtractVectors(position_j,position_i)));
  return acceleration_computed;
}

void resolveCollisions() {
  int i,j;
  for(i=0;i<bodies-1;i++) {
    for(j=i+1;j<bodies;j++){
      if(positions[i].x==positions[j].x && positions[i].y==positions[j].y && positions[i].z==positions[j].z){
        vector temp = velocities[i];
        velocities[i] = velocities[j];
        velocities[j] = temp;
      }
    }
  }
}

void computeAccelerations() {
  int i,j;
  for(i=0;i<bodies;i++){
    accelerations[i].x = 0;
    accelerations[i].y = 0;
    accelerations[i].z = 0;
    for(j=0;j<bodies;j++){
      if(i!=j){
        accelerations[i] = numerical_kernel1(masses[j], positions[i], positions[j], accelerations[i]);
      }
    }
  }
}

void computePositions() {
  int i;

  for(i=0;i<bodies;i++)
    positions[i] = addVectors(positions[i],addVectors(velocities[i],scaleVector(0.5,accelerations[i])));
}

void simulate() {
  computeAccelerations();
  computePositions();
  numerical_kernel2();
  resolveCollisions();
}

int main(int argc,char* argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = atoi(argv[2]);
  
  char* base1 = "../../results/blackbox/plotting/nbodyKer1_"; 
  char* extension = ".txt";
  char fileSpec[strlen(base1)+strlen(argv[1])+strlen(extension)+1];
  snprintf( fileSpec, sizeof( fileSpec ), "%s%s%s", base1, argv[1], extension );
  fptr_kernel1 = fopen(fileSpec,"w");

  char* base2 = "../../results/blackbox/plotting/nbodyKer2_"; 
  snprintf( fileSpec, sizeof( fileSpec ), "%s%s%s", base2, argv[1], extension );
  fptr_kernel2 = fopen(fileSpec,"w");
  double time_spent;
  clock_t begin = clock();
  srand (seed);

/* =============================== */
  do {
/* Generating ranges randomly */
    masses[0] = get_random(0.9, 1.1);
    positions[0].x = get_random(0.0, 0.5);
    positions[0].y = get_random(0.0, 0.5);
    positions[0].z = get_random (0.0, 0.5);
    velocities[0].x = get_random (0.001, 0.019);
    velocities[0].y = get_random (0.0, 0.5);
    velocities[0].z = get_random (0.0, 0.5);
    masses[1] = get_random (0.001, 0.9);
    positions[1].x = get_random (0.9, 1.1);
    positions[1].y = get_random (0.9, 1.1);
    positions[1].z = get_random (0.0, 0.5);
    velocities[1].x = get_random (0.0, 0.5);
    velocities[1].y = get_random (0.0, 0.5);
    velocities[1].z = get_random (0.01, 0.03);
    masses[2] = get_random (0.0001, 0.0019);
    positions[2].x = get_random (0.0, 0.5);
    positions[2].y = get_random (0.9, 1.1);
    positions[2].z = get_random (0.9, 1.1);
    velocities[2].x = get_random (0.001, 0.019);
    velocities[2].y = get_random (-0.019, -0.001);
    velocities[2].z = get_random (-0.019, -0.001);
/* =========================================*/
    for (int k = 0; k < timeSteps; k++) {
      simulate();
	  }
    /* Stops the execution after specified time */
    clock_t end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  } while (time_spent <= maxTime); 

  fclose(fptr_kernel1);
  fclose(fptr_kernel2);
/* ========================================================= */
  return 0;
}

