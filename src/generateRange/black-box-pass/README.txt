Dependencies
-LLVM 10
-CMAKE
-rust (install with curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh)
-rustfilt tool (if rust will be used) (install with 'cargo install rustfilt')

Installation Instructions
-From black-box-pass directory run 'cmake -DLT_LLVM_INSTALL_DIR=(llvm install directory)/LLVM/llvm-project/build'
-Then run 'make'
-To build all test cases run 'build-benchmarks.sh time' where time is in minutes-To run all test cases then run 'run-benchmarks.sh'
(the scripts need to run from the scripts directory)

Config File Instructions

The configuration file should list the ranges on each variable in the original 
main function of the program.  If the first arguement is a numerical type this
is done by adding main_1_min = MIN_NUMBER and main_1_max = MAX_NUMBER.  Likewise
for arguments 2,...,n.  If an argument is an array or pointer type the size must
be given with main_1_size = 4 which says the first argument is a szie 4 array.
Then the range on each element in the array can also be specified by saying 
main_1_0_max = MAX_NUMBER which gives the max value that the element in array 
position 0 can take.  For higher dimensional arrays bounds such as 
main_1_0_3_max can be given.  Finally for arrays in kernel function size 
arguements are specified the same way such as numerical_kernel1_1_size=8 says
that kernel1 takes a size 8 array as it's first argument.
