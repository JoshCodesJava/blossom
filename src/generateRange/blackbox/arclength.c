#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h> 

/* global variables to store the ranges of inputs */
double input, input_min, input_max;
bool initial = true;
FILE *fptr;

/* Generating random intergers */
int get_random(int min, int max){
   return (min + (rand() % (max - min)));
}

double numerical_kernel(double x) {
/* Generating the ranges of the inputs to the kernel function */
  if (initial) {
    input_min = input;
    input_max = input;
    initial = false;
  } else {
    if (input_min > input) {
      input_min = input;
    }
    if (input_max < input) {
      input_max = input;
    }
  }
/* =============================================================*/   
  int k, n = 5;
  double t1;
  double d1 = 1.0L;
  t1 = x;
  for( k = 1; k <= n; k++ ) {
    d1 = 2.0 * d1;
    t1 = t1 + sin (d1 * x) / d1;
  }
  return t1;
}


int main(int argc, char *argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = 60 * atoi(argv[2]);
  char filename[128];
  double time_spent;
  clock_t time_begin = clock();
  srand(seed);
  /* ==================================================== */
  int n, i;
  do {
    n = get_random(1, 1000000); // generating program inputs randomly
    double h, t2, dppi;
    double s1;
    double t1 = -1.0;
    dppi = acos(t1);
    s1 = 0.0;
    t1 = 0.0;
    h = dppi / n;
    for( i = 1; i <= n; i++ ) {
      input = i * h;
      t2 = numerical_kernel(input);
      s1 = s1 + sqrt (h*h + (t2 - t1)*(t2 - t1));
      t1 = t2;
    }

/* Stops the execution after specified mins and prints the ranges in the file */
    clock_t time_end = clock();
    time_spent = ((double)(time_end - time_begin)) / CLOCKS_PER_SEC;
  } while(time_spent <= maxTime);
  fptr = fopen("kernel_range.dat", "w");
  fprintf(fptr,"%.20e %.20e\n",input_min, input_max);
  fclose(fptr);
/* ========================================================= */

  return 0;

}
