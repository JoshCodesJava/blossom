#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#define cot(x) (1.0 / tan(x))

#define TRUE  1
#define FALSE 0

#define max_surfaces 10
#define ITERATIONS 5

#define fabs(x)  ((x < 0.0) ? -x : x)

#define pic 3.1415926535897932

char tbfr[132];

short current_surfaces;
short paraxial;

double clear_aperture;

double aberr_lspher;
double aberr_osc;
double aberr_lchrom;

double max_lspher;
double max_osc;
double max_lchrom;

double radius_of_curvature;
double object_distance;
double ray_height;
double axis_slope_angle;
double from_index;
double to_index;
double spectral_line[9];


double s[max_surfaces][5];
double od_sa[2][2];

/* global variables to store the ranges of inputs */
bool initial_ker1 = true;
bool initial_ker2 = true;
bool initial_ker3 = true;
double x_min[2], x_max[2];
double iang_sin_min[2], iang_sin_max[2];
double from_index_min[3], from_index_max[3];
double to_index_min[3], to_index_max[3];
double slope_angle_min[3], slope_angle_max[3];
double obj_dist_min[2], obj_dist_max[2];
double rad_curvature_min, rad_curvature_max;
double old_ray_height_max, old_ray_height_min;
FILE *fptr1, *fptr2, *fptr3;

/* Generating random doubles */
double get_random(double lowerBound, double upperBound){
  double f = (double)rand() / RAND_MAX;
  return lowerBound + f * (upperBound - lowerBound);
}


char outarr[8][80];
int itercount;
int niter = ITERATIONS;

char *refarr[] = {

        "   Marginal ray          47.09479120920   0.04178472683",
        "   Paraxial ray          47.08372160249   0.04177864821",
        "Longitudinal spherical aberration:        -0.01106960671",
        "    (Maximum permissible):                 0.05306749907",
        "Offense against sine condition (coma):     0.00008954761",
        "    (Maximum permissible):                 0.00250000000",
        "Axial chromatic aberration:                0.00448229032",
        "    (Maximum permissible):                 0.05306749907"
};

double pi = pic;
double twopi = pic * 2.0;
double piover4 = pic / 4.0;
double fouroverpi = 4.0 / pic;
double piover2 = pic / 2.0;

double atanc[] = {
  0.0,
  0.4636476090008061165,
  0.7853981633974483094,
  0.98279372324732906714,
  1.1071487177940905022,
  1.1902899496825317322,
  1.2490457723982544262,
  1.2924966677897852673,
  1.3258176636680324644
};

void numerical_kernel1(double *iang_sin, double from_index, double to_index,
   double slope_angle, double obj_dist, double old_ray_height,
     double *rang_sin, double *old_axis_slope_angle) {
  /* Generating the ranges of the inputs to the kernel function */
  if (initial_ker1) {
    from_index_min[0] = from_index;
    from_index_max[0] = from_index;
    to_index_min[0] = to_index;
    to_index_max[0] = to_index;
    iang_sin_min[0] = (*iang_sin);
    iang_sin_max[0] = (*iang_sin);
    slope_angle_min[0] = slope_angle;
    slope_angle_max[0] = slope_angle;
    obj_dist_min[0] = obj_dist;
    obj_dist_max[0] = obj_dist;
    old_ray_height_max = old_ray_height;
    old_ray_height_min = old_ray_height;
    initial_ker1 = false;
  } else {
    if (from_index_min[0] > from_index) {
      from_index_min[0] = from_index;
    }
    if (from_index_max[0] < from_index) {
      from_index_max[0] = from_index;
    }
    if (to_index_min[0] > to_index) {
      to_index_min[0] = to_index;
    }
    if (to_index_max[0] < to_index) {
      to_index_max[0] = to_index;
    }
    if (iang_sin_min[0] > (*iang_sin)) {
      iang_sin_min[0] = (*iang_sin);
    }
    if (iang_sin_max[0] < (*iang_sin)) {
      iang_sin_max[0] = (*iang_sin);
    }
    if (slope_angle_min[0] > slope_angle) {
      slope_angle_min[0] = slope_angle;
    }
    if(slope_angle_max[0] < slope_angle) {
      slope_angle_max[0] = slope_angle;
    }
    if (obj_dist_min[0] > obj_dist) {
      obj_dist_min[0] = obj_dist;
    }
    if (obj_dist_max[0] < obj_dist) {
      obj_dist_max[0] = obj_dist;
    }
    if (old_ray_height_min > old_ray_height) {
      old_ray_height_min = old_ray_height;
    }
    if (old_ray_height_max < old_ray_height) {
      old_ray_height_max = old_ray_height;
    }
  }
/* =========================================================== */
   *rang_sin = (from_index / to_index) * (*iang_sin);

   *old_axis_slope_angle = slope_angle;
   axis_slope_angle = slope_angle + (*iang_sin) - (*rang_sin);
   if (obj_dist != 0.0) {
     ray_height = obj_dist * (*old_axis_slope_angle);
   }
   object_distance = ray_height / axis_slope_angle;
}


void numerical_kernel2(double *iang_sin, double from_index, double to_index,
   double slope_angle, double rad_curvature,
     double *iang, double *rang_sin, double *old_axis_slope_angle, double *sagitta) {
  /* Generating the ranges of the inputs to the kernel function */
  if (initial_ker2) {
    from_index_min[1] = from_index;
    from_index_max[1] = from_index;
    to_index_min[1] = to_index;
    to_index_max[1] = to_index;
    iang_sin_min[1] = (*iang_sin);
    iang_sin_max[1] = (*iang_sin);
    slope_angle_min[1] = slope_angle;
    slope_angle_max[1] = slope_angle;
    rad_curvature_min = rad_curvature;
    rad_curvature_max = rad_curvature;
    initial_ker2 = false;
  } else {
    if (from_index_min[1] > from_index) {
      from_index_min[1] = from_index;
    }
    if (from_index_max[1] < from_index) {
      from_index_max[1] = from_index;
    }
    if (to_index_min[1] > to_index) {
      to_index_min[1] = to_index;
    }
    if (to_index_max[1] < to_index) {
      to_index_max[1] = to_index;
    }
    if (iang_sin_min[1] > (*iang_sin)) {
      iang_sin_min[1] = (*iang_sin);
    }
    if (iang_sin_max[1] < (*iang_sin)) {
      iang_sin_max[1] = (*iang_sin);
    }
    if (slope_angle_min[1] > slope_angle) {
      slope_angle_min[1] = slope_angle;
    }
    if(slope_angle_max[1] < slope_angle) {
      slope_angle_max[1] = slope_angle;
    }
    if (rad_curvature_min > rad_curvature) {
      rad_curvature_min = rad_curvature;
    }
    if (rad_curvature_max < rad_curvature) {
      rad_curvature_max = rad_curvature;
    }
  }
/* =========================================================== */
  *iang = asin(*iang_sin);
  *rang_sin = (from_index / to_index) * (*iang_sin);

  *old_axis_slope_angle = slope_angle;
  axis_slope_angle = slope_angle + (*iang) - asin(*rang_sin);
  *sagitta = sin((*old_axis_slope_angle + (*iang)) / 2.0);
  *sagitta = 2.0 * radius_of_curvature*(*sagitta)*(*sagitta);
  object_distance = ((rad_curvature * sin(*old_axis_slope_angle + (*iang))) *
               cot(axis_slope_angle)) + (*sagitta);
}

void numerical_kernel3(double from_index, double to_index,
   double slope_angle, double obj_dist,
     double *rang) {
  /* Generating the ranges of the inputs to the kernel function */
  if (initial_ker3) {
    from_index_min[2] = from_index;
    from_index_max[2] = from_index;
    to_index_min[2] = to_index;
    to_index_max[2] = to_index;
    slope_angle_min[2] = slope_angle;
    slope_angle_max[2] = slope_angle;
    obj_dist_min[1] = obj_dist;
    obj_dist_max[1] = obj_dist;
    initial_ker3 = false;
  } else {
    if (from_index_min[2] > from_index) {
      from_index_min[2] = from_index;
    }
    if (from_index_max[2] < from_index) {
      from_index_max[2] = from_index;
    }
    if (to_index_min[2] > to_index) {
      to_index_min[2] = to_index;
    }
    if (to_index_max[2] < to_index) {
      to_index_max[2] = to_index;
    }
    if (slope_angle_min[2] > slope_angle) {
      slope_angle_min[2] = slope_angle;
    }
    if(slope_angle_max[2] < slope_angle) {
      slope_angle_max[2] = slope_angle;
    }
    if (obj_dist_min[1] > obj_dist) {
      obj_dist_min[1] = obj_dist;
    }
    if (obj_dist_max[1] < obj_dist) {
      obj_dist_max[1] = obj_dist;
    }
  }
/* =========================================================== */
  *rang = -asin((from_index / to_index) * sin(slope_angle));
  object_distance = obj_dist * ((to_index * cos(-(*rang))) /
     (from_index * cos(slope_angle)));
  axis_slope_angle = -(*rang);
}

void transit_surface() {
  double iang,
         rang,
         iang_sin,
         rang_sin,
         old_axis_slope_angle, sagitta;
  if (paraxial) {
    if (radius_of_curvature != 0.0) {
      if (object_distance == 0.0) {
        axis_slope_angle = 0.0;
        iang_sin = ray_height / radius_of_curvature;
      } else {
        iang_sin = ((object_distance -
            radius_of_curvature) / radius_of_curvature) * axis_slope_angle;
      }

      numerical_kernel1(&iang_sin, from_index, to_index, axis_slope_angle,
      object_distance, ray_height, &rang_sin, &old_axis_slope_angle);
      return;
    }
    object_distance = object_distance * (to_index / from_index);
    axis_slope_angle = axis_slope_angle * (from_index / to_index);
    return;
  }

  if (radius_of_curvature != 0.0) {
    if (object_distance == 0.0) {
      axis_slope_angle = 0.0;
      iang_sin = ray_height / radius_of_curvature;
    } else {
      iang_sin = ((object_distance - radius_of_curvature) /
        radius_of_curvature) * sin(axis_slope_angle);
    }
    numerical_kernel2(&iang_sin, from_index, to_index, axis_slope_angle,
      radius_of_curvature, &iang, &rang_sin, &old_axis_slope_angle, &sagitta);
    return;
  }
  numerical_kernel3(from_index, to_index, axis_slope_angle, object_distance, &rang);        
}

void trace_line(int line, double ray_h, double spectral_line[]) {
  int i;

  object_distance = 0.0;
  ray_height = ray_h;
  from_index = 1.0;
  for (i = 1; i <= current_surfaces; i++) {
    radius_of_curvature = s[i][1];
    to_index = s[i][2];
    if (to_index > 1.0) {
      to_index = to_index + ((spectral_line[4] -
          spectral_line[line]) /
            (spectral_line[3] - spectral_line[6])) * ((s[i][2] - 1.0) /
              s[i][3]);
    }
    transit_surface();
    from_index = to_index;
    if (i < current_surfaces) {
      object_distance = object_distance - s[i][4];
    }
  }
}

int main(int argc, char *argv[]) {
  /* getting the seed and time as command line arguments */
  if(argc <= 2) {
    printf("Expects 2 arguments!\n");
    exit(1);
  }
  int seed = atoi(argv[1]);
  int maxTime = 60 * atoi(argv[2]);
  double time_spent;
  clock_t time_begin = clock();
  srand(seed);
  /* ==================================================== */
  int i, j, k, errors;
  double od_fline, od_cline;
  long passes;
  do {
  /* Generating ranges randomly */
    spectral_line[1] = get_random(7521.0, 7621.0);
    spectral_line[2] = get_random(6769.955, 6869.955);
    spectral_line[3] = get_random(6462.816, 6562.816);
    spectral_line[4] = get_random(5795.944, 5895.944);
    spectral_line[5] = get_random(5169.557, 5269.557);
    spectral_line[6] = get_random(4761.344, 4861.344);
    spectral_line[7] = get_random(4240.477, 4340.477);
    spectral_line[8] = get_random(3868.494, 3968.494);
  /* =========================================*/
    clear_aperture = 4.0;
    s[1][1] = 27.05;
    s[1][2] = 1.5137;
    s[1][3] = 63.6;
    s[1][4] = 0.52;
    s[2][1] = -16.68;
    s[2][2] = 1;
    s[2][3] = 0;
    s[2][4] = 0.138;
    s[3][1] = -16.68;
    s[3][2] = 1.6164;
    s[3][3] = 36.7;
    s[3][4] = 0.38;
    s[4][1] = -78.1;
    s[4][2] = 1;
    s[4][3] = 0;
    s[4][4] = 0;
    passes = 0;
    current_surfaces = 4;

    while(passes < niter ) {
      passes++;
      for (itercount = 0; itercount < niter; itercount++) {
        for (paraxial = 0; paraxial <= 1; paraxial++) {
          trace_line(4, clear_aperture / 2.0, spectral_line);
          od_sa[paraxial][0] = object_distance;
          od_sa[paraxial][1] = axis_slope_angle;
        }
        paraxial = FALSE;


        trace_line(3, clear_aperture / 2.0, spectral_line);
        od_cline = object_distance;


        trace_line(6, clear_aperture / 2.0, spectral_line);
        od_fline = object_distance;

        aberr_lspher = od_sa[1][0] - od_sa[0][0];
        aberr_osc = 1.0 - (od_sa[1][0] * od_sa[1][1]);
        aberr_lchrom = od_fline - od_cline;
        max_lspher = sin(od_sa[0][1]);

        max_lspher = 0.0000926;
        max_osc = 0.0025;
        max_lchrom = max_lspher;
      }

      errors = 0;
      for (i = 0; i < 8; i++) {
        if (strcmp(outarr[i], refarr[i]) != 0) {
          k = strlen(refarr[i]);
          for (j = 0; j < k; j++) {
            if (refarr[i][j] != outarr[i][j]) {
              errors++;
            }
          }
        }
      }
    }
    /* Stops the execution after specified mins and prints the ranges in the file */
    clock_t time_end = clock();
    time_spent = ((double)(time_end - time_begin)) / CLOCKS_PER_SEC;
  } while (time_spent <= maxTime); 
  fptr1 = fopen("kernel1_range.dat", "w");
  fptr2 = fopen("kernel2_range.dat", "w");
  fptr3 = fopen("kernel3_range.dat", "w");

  fprintf(fptr1, "%.20e %.20e\n",
    iang_sin_min[0], iang_sin_max[0]);
  fprintf(fptr1, "%.20e %.20e\n",
    from_index_min[0], from_index_max[0]);
  fprintf(fptr1, "%.20e %.20e\n",
    to_index_min[0], to_index_max[0]);
  fprintf(fptr1, "%.20e %.20e\n",
    slope_angle_min[0], slope_angle_max[0]);
  fprintf(fptr1, "%.20e %.20e\n",
    obj_dist_min[0], obj_dist_max[0]);
  fprintf(fptr1, "%.20e %.20e\n",
    old_ray_height_min, old_ray_height_max);

  fprintf(fptr2, "%.20e %.20e\n",
    iang_sin_min[1], iang_sin_max[1]);
  fprintf(fptr2, "%.20e %.20e\n",
    from_index_min[1], from_index_max[1]);
  fprintf(fptr2, "%.20e %.20e\n",
    to_index_min[1], to_index_max[1]);
  fprintf(fptr2, "%.20e %.20e\n",
    slope_angle_min[1], slope_angle_max[1]);
  fprintf(fptr2, "%.20e %.20e\n",
    rad_curvature_min, rad_curvature_max);

  fprintf(fptr3, "%.20e %.20e\n",
    from_index_min[2], from_index_max[2]);
  fprintf(fptr3, "%.20e %.20e\n",
    to_index_min[2], to_index_max[2]);
  fprintf(fptr3, "%.20e %.20e\n",
    slope_angle_min[2], slope_angle_max[2]);
  fprintf(fptr3, "%.20e %.20e\n",
    obj_dist_min[1], obj_dist_max[1]);

  fclose(fptr1); 
  fclose(fptr2); 
  fclose(fptr3); 

  return 0;
}

