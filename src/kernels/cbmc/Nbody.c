#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>

double velocities_x[3], velocities_y[3], velocities_z[3];
double GravConstant = 0.01;
double new_accel[3];
  
void numerical_kernel1() {
  double a[3]; 
  double position_i[3], position_j[3], acceleration[3];
  double mass;
  int i;
  __CPROVER_assume(position_i[0] >= -86545.0148931074 && position_i[0] <= 150146.55756686);
  __CPROVER_assume(position_i[1] >= -106612.970838932 && position_i[1] <= 104328.238959767);
  __CPROVER_assume(position_i[2] >= -104129.64891511 && position_i[2] <= 121900.073601499);
  __CPROVER_assume(position_j[0] >= -86545.0148931074 && position_j[0] <= 150146.55756686); 
  __CPROVER_assume(position_j[1] >= -106612.970838932 && position_j[1] <= 104328.238959767);
  __CPROVER_assume(position_j[2] >= -104129.64891511 && position_j[2] <= 121900.073601499);
  __CPROVER_assume(acceleration[0] >= -25257.1955398914 && acceleration[0] <= 26122.2164987853);
  __CPROVER_assume(acceleration[1] >= -49492.2057046081 && acceleration[1] <= 18458.2662415526);
  __CPROVER_assume(acceleration[2] >= -18433.1517005512 && acceleration[2] <= 98210.1684896515);
  __CPROVER_assume(mass >= 0.9 && mass <= 1.1);

  for(i = 0; i<3; i++) {
    a[i] = position_i[i] - position_j[i];
  }
  double mod = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]); 

  double scale = GravConstant*mass/(mod * mod * mod);
  for (i =0; i<3; i++) {
    new_accel[i] = (acceleration[i] + (scale * (position_j[i] - position_i[i])));
    __CPROVER_assert(!(isinf(new_accel[i])), "infinity");
  }
}  


void numerical_kernel2() {
  double velocity_x[3], velocity_y[3], velocity_z[3];
  double accelerations_x[3], accelerations_y[3], accelerations_z[3];
  __CPROVER_assume(velocity_x[0] >= -399.108805043489 && velocity_x[0] <= 140.395874122329);
  __CPROVER_assume(velocity_x[1] >= -197.053728533596 && velocity_x[1] <= 572.207879861596);
  __CPROVER_assume(velocity_x[2] >= -17145.0409716001 && velocity_x[2] <= 23554.068142828);
  __CPROVER_assume(velocity_y[0] >= -193.619502420673 && velocity_y[0] <= 269.94727755118);
  __CPROVER_assume(velocity_y[1] >= -452.770478818149 && velocity_y[1] <= 282.837886313683);
  __CPROVER_assume(velocity_y[2] >= -17481.5831794456 && velocity_y[2] <= 18458.1956527784);
  __CPROVER_assume(velocity_z[0] >= -394.13670682791 && velocity_z[0] <= 301.590181598338);
  __CPROVER_assume(velocity_z[1] >= -459.964415984446 && velocity_z[1] <= 605.252803824391);
  __CPROVER_assume(velocity_z[2] >= -18433.2758809848 && velocity_z[2] <= 19029.6301890438);
  __CPROVER_assume(accelerations_x[0] >= -410.139852063405 && accelerations_x[0] <= 412.074434438454);
  __CPROVER_assume(accelerations_x[1] >= -773.374617254222 && accelerations_x[1] <= 596.566959341908);
  __CPROVER_assume(accelerations_x[2] >= -25257.1939468974 && accelerations_x[2] <= 26122.2189362439);
  __CPROVER_assume(accelerations_y[0] >= -209.228821604021 && accelerations_y[0] <= 303.405888438039);
  __CPROVER_assume(accelerations_y[1] >= -477.258175305645 && accelerations_y[1] <= 291.977303179275);
  __CPROVER_assume(accelerations_y[2] >= -49492.2053148291 && accelerations_y[2] <= 18458.266842774);
  __CPROVER_assume(accelerations_z[0] >= -397.851206364176 && accelerations_z[0] <= 503.398256724025);
  __CPROVER_assume(accelerations_z[1] >= -979.818814739621 && accelerations_z[1] <= 632.265331759936);
  __CPROVER_assume(accelerations_z[2] >= -18433.1523198009 && accelerations_z[2] <= 98210.1674057562);
  for(int i = 0; i<3; i++){
    velocities_x[i] = velocity_x[i] + accelerations_x[i];
    velocities_y[i] = velocity_y[i] + accelerations_y[i];
    velocities_z[i] = velocity_z[i] + accelerations_z[i];
    __CPROVER_assert(!(isinf(velocities_x[i])), "infinity");
    __CPROVER_assert(!(isinf(velocities_y[i])), "infinity");
    __CPROVER_assert(!(isinf(velocities_z[i])), "infinity");

  }
}

int main(void) { 
  numerical_kernel();
  numerical_kernel();
  return 0;
}

