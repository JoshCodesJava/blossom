#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include "klee/klee.h"


double numerical_kernel(double x) {
  int k, n = 5;
  double t1;
  double d1 = 1.0L;
  t1 = x;
  for( k = 1; k <= n; k++ ) {
    d1 = 2.0 * d1;
    t1 = t1 + sin (d1 * x) / d1;
    assert(!isinf(t1));
    assert(!isnan(t1));
  }

  return t1;
}

int main(void) {
  int n;
  klee_make_symbolic(&n, sizeof(int), "n");
  klee_prefer_cex(n, 1 <= n && n <= 1000000);
  int i;
  double h, t2, dppi;
  double s1;
  double t1 = -1.0;
  dppi = acos(t1);
  s1 = 0.0;
  t1 = 0.0;
  h = dppi / n;
  for( i = 1; i <= n; i++ ) {
    t2 = numerical_kernel(i * h); 
    s1 = s1 + sqrt (h*h + (t2 - t1)*(t2 - t1));
    t1 = t2;
  }
  return 0;
}
